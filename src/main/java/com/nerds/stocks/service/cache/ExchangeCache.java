package com.nerds.stocks.service.cache;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.nerds.stocks.data.Exchange;
import com.nerds.stocks.data.Holiday;
import com.nerds.stocks.db.entities.ExchangeEntity;
import com.nerds.stocks.db.repos.ExchangeRepository;

@Service
public class ExchangeCache {

    @Autowired
    ExchangeRepository exchangeRepository;

    List<ExchangeEntity> exchanges;

    @PostConstruct
    @Async
    public void loadExchanges() {
        createInitialExchanges();
        exchanges = exchangeRepository.findAll();

    }

    public Exchange getExchange(String exchangeId) {
        Optional<ExchangeEntity> entity = exchanges.parallelStream()
                .filter(exchangeEntity -> exchangeEntity.getExchangeId().equalsIgnoreCase(exchangeId)).findFirst();

        if (entity.isPresent()) {
            return new Exchange(entity.get());
        } else {
            Exchange exchange = new Exchange();
            exchange.setExchangeId(exchangeId);
            return exchange;
        }

    }

    public void createInitialExchanges() {
        Exchange NYSE = new Exchange("NYSE", "New York Stock Exchange", "New York Stock Exchange", "Manhattan",
                "New York", "USA", 9, 30, 16, 0, 18, 0, TimeZone.getTimeZone("EST"), getUsaHolidaysList());
        if (!exchangeRepository.findById("NYSE").isPresent())
            exchangeRepository.save(NYSE.toExchangeEntity());
        
        
        Exchange NASDAQ = new Exchange("NASDAQ", "National Association of Securities Dealers Automated Quotations",
                "National Association of Securities Dealers Automated Quotations", "Manhattan", "New York", "USA", 9,
                30, 16, 0, 18, 0,TimeZone.getTimeZone("EST"), getUsaHolidaysList());
        if (!exchangeRepository.findById("NASDAQ").isPresent())
            exchangeRepository.save(NASDAQ.toExchangeEntity());
    }

    public List<Holiday> getUsaHolidaysList() {
        Calendar.Builder builder = new Calendar.Builder();
        Holiday[] usaHolidays = {
                // New Year day
                new Holiday("New Year",
                        builder.set(Calendar.MONTH, Calendar.JANUARY).set(Calendar.DAY_OF_MONTH, 1).build()),
                new Holiday("Martin Luther King Jr. Day",
                        builder.set(Calendar.MONTH, Calendar.JANUARY).set(Calendar.DAY_OF_WEEK_IN_MONTH, 3)
                                .set(Calendar.DAY_OF_WEEK, Calendar.MONDAY).build()), // Martin Luther King Jr. Day
                new Holiday("Washington's Birthday",
                        builder.set(Calendar.DAY_OF_WEEK_IN_MONTH, 3).set(Calendar.MONTH, Calendar.FEBRUARY)
                                .set(Calendar.DAY_OF_WEEK, Calendar.MONDAY).build()), // Washington's Birthday
                new Holiday("Memorial Day",
                        builder.set(Calendar.DAY_OF_WEEK_IN_MONTH, -1).set(Calendar.MONTH, Calendar.MAY)
                                .set(Calendar.DAY_OF_WEEK, Calendar.MONDAY).build()), // Memorial Day
                new Holiday("Independence Day",
                        builder.set(Calendar.MONTH, Calendar.JULY).set(Calendar.DAY_OF_MONTH, 4).build()), // Independence
                                                                                                           // Day
                new Holiday("Labor Day",
                        builder.set(Calendar.DAY_OF_WEEK_IN_MONTH, 1).set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
                                .set(Calendar.MONTH, Calendar.SEPTEMBER).build()), // Labor Day
                new Holiday("Thanksgiving",
                        builder.set(Calendar.DAY_OF_WEEK_IN_MONTH, 3).set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY)
                                .set(Calendar.MONTH, Calendar.NOVEMBER).build()), // Thanksgiving
                new Holiday("Christmas",
                        builder.set(Calendar.MONTH, Calendar.DECEMBER).set(Calendar.DAY_OF_MONTH, 25).build()) // Christmas
        };
        return Arrays.asList(usaHolidays);
    }

}