package com.nerds.stocks.db.repos;

import javax.transaction.Transactional;

import com.nerds.stocks.db.entities.DailyQuoteEntity;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@Transactional
@RepositoryRestResource(collectionResourceRel = "dailyQuotes", path = "dailyQuotes")
public interface DailyQuotesRepository extends QuotesBaseRepository<DailyQuoteEntity>{
    
}