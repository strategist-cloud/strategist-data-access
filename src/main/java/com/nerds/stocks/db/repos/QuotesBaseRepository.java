package com.nerds.stocks.db.repos;

import java.sql.Timestamp;
import java.util.List;

import com.nerds.stocks.db.entities.QuoteEntity;
import com.nerds.stocks.db.entities.StockEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface QuotesBaseRepository<T extends QuoteEntity> extends JpaRepository<T, Long>{
    @Query(value = "select quote from #{#entityName} as quote where quote.stock.symbol = :ticker and quote.endTime BETWEEN :from AND :to")
    public List<T> getQuotesFor(String ticker, Timestamp from, Timestamp to);

    @Query(value = "SELECT quote.stock from #{#entityName} as quote")
    public List<StockEntity> getStocks();

    @Query(value = "select quote from #{#entityName} as quote where quote.stock.symbol = :ticker and quote.endTime > :from ORDER BY quote.endTime")
    public List<T> getQuotesFrom(String ticker, Timestamp from);

    @Query(value = "select quote from #{#entityName} as quote where quote.id IN (SELECT MAX(childQuote.id) from #{#entityName} as childQuote group by childQuote.stock)")
    public List<T> getLatestRecords();
}