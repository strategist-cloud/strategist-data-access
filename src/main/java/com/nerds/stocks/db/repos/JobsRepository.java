package com.nerds.stocks.db.repos;

import javax.transaction.Transactional;

import com.nerds.stocks.db.entities.JobsEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@Transactional
@RepositoryRestResource(collectionResourceRel="jobs", path="jobs")
public interface JobsRepository extends JpaRepository<JobsEntity, String> {
    enum JOB_NAMES {
        FIVE_MIN_QUOTE_UPDATE("FIVE_MIN_QUOTE_UPDATE"), THIRTY_MIN_QUOTE_UPDATE("THIRTY_MIN_QUOTE_UPDATE"), 
        DAILY_QUOTE_UPDATE("DAILY_QUOTE_UPDATE"), WEEKLY_QUOTE_UPDATE("WEEKLY_QUOTE_UPDATE");

        String name;

        JOB_NAMES(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}