package com.nerds.stocks.db.repos;

import javax.transaction.Transactional;

import com.nerds.stocks.db.entities.PortfolioEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@Transactional
@RepositoryRestResource(collectionResourceRel = "portfolios", path = "portfolios")
public interface PortfolioRepository extends JpaRepository<PortfolioEntity, Long>{
    
    @Query(value = "SELECT portfolio from PortfolioEntity as portfolio WHERE portfolio.user.userName = :userName AND portfolio.portfolioName = :portfolioName")
    public PortfolioEntity findByNameAndUser(String portfolioName, String userName);

}