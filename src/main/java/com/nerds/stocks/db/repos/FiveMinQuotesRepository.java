package com.nerds.stocks.db.repos;

import javax.transaction.Transactional;

import com.nerds.stocks.db.entities.FiveMinQuoteEntity;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@Transactional
@RepositoryRestResource(collectionResourceRel = "fiveMinQuotes", path = "fiveMinQuotes")
public interface FiveMinQuotesRepository extends QuotesBaseRepository<FiveMinQuoteEntity> {

}