package com.nerds.stocks.db.repos;

import javax.transaction.Transactional;

import com.nerds.stocks.db.entities.ThirtyMinQuoteEntity;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@Transactional
@RepositoryRestResource(collectionResourceRel = "thirtyMinQuotes", path = "thirtyMinQuotes")
public interface ThirtyMinQuotesRepository  extends QuotesBaseRepository<ThirtyMinQuoteEntity>{

}