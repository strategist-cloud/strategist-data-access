package com.nerds.stocks.db.repos;

import java.util.List;

import javax.transaction.Transactional;

import com.nerds.stocks.db.entities.QuoteEntity;
import com.nerds.stocks.db.entities.RecentQuoteEntity;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@Transactional
@RepositoryRestResource(collectionResourceRel = "quotes", path = "quotes")
@CrossOrigin(origins = "http://localhost:4200")
public interface QuotesRepository extends QuotesBaseRepository<RecentQuoteEntity> {
    // @Query(value = "Select quote from RecentQuoteEntity as quote where
    // stock.symbol = :ticker ORDER BY endTime desc")
    // public List<QuoteEntity> getPrice(String ticker);

    @Query(value = "Select quote from RecentQuoteEntity as quote where stock.exchange = :exchange and stock.symbol = :ticker ORDER BY endTime desc")
    public List<QuoteEntity> getPrice(String exchange, String ticker);
}