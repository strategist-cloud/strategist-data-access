package com.nerds.stocks.db.repos;

import javax.transaction.Transactional;

import com.nerds.stocks.db.entities.StockEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@Transactional
@RepositoryRestResource(collectionResourceRel = "stocks", path = "stocks")
@CrossOrigin(origins = "http://localhost:4200")
public interface StocksRepository extends JpaRepository<StockEntity, Long> {
    final String stocksTableName = "SELECT stock from " + StockEntity.class.getSimpleName() + " as stock";

    // @Query(value = "SELECT stock from StockEntity as stock WHERE stock.symbol =
    // :ticker")
    // public StockEntity get(String ticker);

    @Query(value = "SELECT stock from StockEntity as stock WHERE stock.exchange = :exchange AND stock.symbol = :ticker")
    public StockEntity get(String exchange, String ticker);
}