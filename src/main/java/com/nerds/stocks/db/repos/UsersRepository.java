package com.nerds.stocks.db.repos;

import java.util.List;

import javax.transaction.Transactional;

import com.nerds.stocks.db.entities.UserEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@Transactional
@RepositoryRestResource(collectionResourceRel = "users", path = "users")
@CrossOrigin(origins = "http://localhost:4200")
public interface UsersRepository extends JpaRepository<UserEntity, Long>{
    List<UserEntity> findByUserName(String userName);
}