package com.nerds.stocks.db.repos;

import com.nerds.stocks.db.entities.TransactionEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@RepositoryRestResource(collectionResourceRel = "transactions", path = "transactions")
public interface TransactionsRepository extends JpaRepository<TransactionEntity, Long> {
    @Query("DELETE FROM TransactionEntity as transaction WHERE transaction.user.userName = :userName AND transaction.portfolio.portfolioName = :portfolioName AND transaction.stock.exchange = :exchange AND transaction.stock.symbol = :ticker")
    public void remove(String portfolioName, String userName, String exchange, String ticker);
}