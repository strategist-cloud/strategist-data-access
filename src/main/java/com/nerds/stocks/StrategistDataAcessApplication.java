package com.nerds.stocks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StrategistDataAcessApplication {

	public static void main(String[] args) {
		SpringApplication.run(StrategistDataAcessApplication.class, args);
	}

}
